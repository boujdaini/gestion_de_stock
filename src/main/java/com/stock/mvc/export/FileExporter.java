package com.stock.mvc.export;

import java.io.File;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

public interface FileExporter {
	
	boolean exportDataToExcel(HttpServletResponse response, String fileName, String encodage);
	
	boolean importDataFromExcel();

}
