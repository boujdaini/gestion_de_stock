package com.stock.mvc.controllers;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import com.stock.mvc.entites.Roles;
import com.stock.mvc.entites.Utilisateur;
import com.stock.mvc.services.IFlickrService;
import com.stock.mvc.services.IRoleService;
import com.stock.mvc.services.IUtilisateurService;

@Controller
@RequestMapping(value = "/user")
public class UserController {
	
	@Autowired
	private IUtilisateurService utilisateurService;
	
	@Autowired
	private IFlickrService flickrService;
	
	@Autowired
	private IRoleService roleService;

	@RequestMapping(value = "/")
	public String user(Model model) {
		List<Utilisateur> users = utilisateurService.selectAll();
		if (users == null) {
			users = new ArrayList<Utilisateur>();
		}
		model.addAttribute("users", users);
		return "users/user";
	}
	
	@RequestMapping(value = "/nouveau", method = RequestMethod.GET)
	public String ajouterUser(Model model) {
		Utilisateur user = new Utilisateur();
		List<Roles> roles = roleService.selectAll();
		if (roles == null) {
			roles = new ArrayList<Roles>();
		}
		model.addAttribute("user", user);
		model.addAttribute("roles", roles);
		return "users/ajouterUser";
	}
	
	@RequestMapping(value = "/enregistrer")
	public String enregistrerUser(Model model, Utilisateur user, MultipartFile file) {
		String photoUrl = null;
		if (user != null) {
			if (file != null && !file.isEmpty()) {
				InputStream stream = null;
				try {
					stream = file.getInputStream();
					photoUrl = flickrService.savePhoto(stream, user.getNom());
					user.setPhoto(photoUrl);
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					try {
						stream.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
			if (user.getIdUtilisateur() != null) {
				utilisateurService.update(user);
			} else {
				utilisateurService.save(user);
			}
		}
		return "redirect:/user/";
	}
	
	@RequestMapping(value = "/modifier/{idUtilisateur}")
	public String modifierUser(Model model, @PathVariable Long idUtilisateur) {
		if (idUtilisateur != null) {
			Utilisateur user = utilisateurService.getById(idUtilisateur);
			List<Roles> roles = roleService.selectAll();
			if (roles == null) {
				roles = new ArrayList<Roles>();
			}
			model.addAttribute("roles", roles);
			if (user != null) {
				model.addAttribute("user", user);
			}
		}
		return "users/ajouterUser";
	}
	
	@RequestMapping(value = "/supprimer/{idUtilisateur}")
	public String supprimerUser(Model model, @PathVariable Long idUtilisateur) {
		if (idUtilisateur != null) {
			Utilisateur user = utilisateurService.getById(idUtilisateur);
			if (user != null) {
				utilisateurService.remove(idUtilisateur);
			}
		}
		return "redirect:/user/";
	}
}
