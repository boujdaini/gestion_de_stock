package com.stock.mvc.dao;

import com.stock.mvc.entites.Roles;

public interface IRoleDao extends IGenericDao<Roles> {

}
